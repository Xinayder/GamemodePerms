package dev.xinayder.gamemode

import java.util.logging.Logger

import org.bukkit.Bukkit
import org.bukkit.event.EventHandler

public class Main() : org.bukkit.plugin.java.JavaPlugin(), org.bukkit.event.Listener {

    companion object {
        public val logger : Logger = Bukkit.getLogger();
    }

    override fun onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this)

    }

    @EventHandler(ignoreCancelled = true)
    public fun onPlayerCommandPreprocessEvent(event: org.bukkit.event.player.PlayerCommandPreprocessEvent) {
        // Checks if the player has enough permissions to change his gamemode
        var rawCommand : String = event.getMessage()

        if (rawCommand.startsWith("/gamemode")) {

            if (rawCommand.contains(" ")) {
                var gamemode : String = rawCommand.removeRange(0, rawCommand.indexOf(" ") + 1)
                if (gamemode.contains(" ")) {
                    gamemode = gamemode.substring(0, rawCommand.indexOf(" "))
                }

                var permission : String = "minecraft.command.gamemode"

                when (gamemode) {
                    "creative" -> permission = permission + ".creative"
                    "adventure" -> permission = permission + ".adventure"
                    "spectator" -> permission = permission + ".spectator"
                    "survival" -> permission = permission + ".survival"
                    else -> {}
                }

                if (permission.contains("creative") || permission.contains("spectator")
                    || permission.contains("adventure") || permission.contains("survival")) {
                    var playerHasPermission : Boolean = event.player.hasPermission(permission)

                    if (!playerHasPermission) {
                        event.setCancelled(true)
                    }
                }
            }
        }
    }
}

